#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 60

    def test_ovmf_ia32_pc_rom_1g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'pc', memory = '1G')
        self.common_add_rom('Firmware/ia32/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_pc_rom_4g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'pc', memory = '4G')
        self.common_add_rom('Firmware/ia32/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_pc_flash_1g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'pc', memory = '1G')
        self.common_add_flash('Firmware/ia32/OVMF_CODE.fd',
                              'Firmware/ia32/OVMF_VARS.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_q35_rom_1g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'q35', memory = '1G')
        self.common_add_rom('Firmware/ia32/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_q35_rom_4g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'q35', memory = '4G')
        self.common_add_rom('Firmware/ia32/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_q35_flash_1g(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'q35', memory = '1G')
        self.common_add_flash('Firmware/ia32/OVMF_CODE.fd',
                              'Firmware/ia32/OVMF_VARS.fd')
        self.common_boot_kernel()
