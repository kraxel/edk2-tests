#!/bin/sh

if test "$VIRTUAL_ENV" != ""; then
    echo "### venv mode ($VIRTUAL_ENV)"
    pipinst="python3 -m pip install"
else
    echo "### user mode"
    pipinst="python3 -m pip install --user"
fi

echo "# upgrade pip"
$pipinst --upgrade pip				|| exit 1

echo "# install python packages"
$pipinst -r pip-requirements.txt		|| exit 1

echo "# (shallow) clone qemu git repo"
git clone --depth=1 \
    https://gitlab.com/qemu/qemu.git		|| exit 1

echo "# install qemu python module"
pushd qemu/python				|| exit 1
$pipinst .					|| exit 1

echo "# cleanup qemu git repo"
popd
rm -rf qemu
