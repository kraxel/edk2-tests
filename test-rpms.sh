#!/bin/sh

cfg="$1"
add="$2"

if test ! -f "$cfg"; then
    echo "usage: $0 /usr/share/qemu/firmware/<file> [ <tag> ]"
    exit 1
fi

unset WORKSPACE
if test "$(jq -r '.mapping.device' $cfg)" = "flash"; then
    export EDK2_CODE=$(jq -r '.mapping.executable.filename' $cfg)
    export EDK2_VARS=$(jq -r '.mapping."nvram-template".filename' $cfg)
fi
if test "$(jq -r '.mapping.device' $cfg)" = "memory"; then
    export EDK2_ROM=$(jq -r '.mapping.filename' $cfg)
fi
export TARGET_ARCH=$(jq -r '.targets[].architecture' $cfg)

# extra tag
if test "$add" != ""; then
    add=",${add}"
fi

if jq -r '.features[]' $cfg | grep enrolled-keys; then
    add="${add},group:sb"
fi

# arch + machine tags
tags=""
if jq -r '.targets[].machines[]' $cfg | grep -q pc-i440fx; then
    tags="$tags -t arch:${TARGET_ARCH},machine:pc${add}"
fi
if jq -r '.targets[].machines[]' $cfg | grep -q pc-q35; then
    tags="$tags -t arch:${TARGET_ARCH},machine:q35${add}"
fi
if jq -r '.targets[].machines[]' $cfg | grep -q microvm; then
    tags="$tags -t arch:${TARGET_ARCH},machine:microvm${add}"
fi
if jq -r '.targets[].machines[]' $cfg | grep -q virt; then
    tags="$tags -t arch:${TARGET_ARCH},machine:virt${add}"
fi

echo "###"
echo "### conf: $cfg"
echo "### code: $EDK2_CODE"
echo "### vars: $EDK2_VARS"
echo "### rom:  $EDK2_ROM"
echo "### arch: $TARGET_ARCH"
echo "### tags: ${tags# }"
echo "###"
avocado run --max-parallel-tasks 4 \
	bios.py $(ls *-{aa64,arm,ia32,x64,ia32x64,riscv64}.py) \
        $tags
rc="$?"
echo "###"
echo "### rc $rc"
echo "###"

exit $rc
