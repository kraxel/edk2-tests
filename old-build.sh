#!/bin/sh
# see https://gitlab.com/kraxel/edk2-build-config for edk2-build.py
set -ex
edk2-build.py --config build.cfg --core ../edk2 --silent --no-logs
