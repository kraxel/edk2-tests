import os
import grp
import time
import subprocess

# avocado
import avocado.utils.process

# local
from Edk2TestLib.core import Edk2TestCore

def group_exists(name: str) -> bool:
    return name in [gr.gr_name for gr in grp.getgrall()]

class Edk2TestNetwork(Edk2TestCore):

    # config
    dev = "net0"

    # state
    unshare = None
    pcapture = None
    dnsmasq = None
    radvd = None
    webserver = None

    def setUp(self):
        Edk2TestCore.setUp(self)
        cmdline = 'unshare -n -U -r'
        helptext = subprocess.run([ 'unshare', '--help' ], stdout = subprocess.PIPE)
        if helptext.stdout.decode().find('--map-group') != -1 and group_exists("wireshark"):
            cmdline += ' --map-group wireshark'
        cmdline += ' sleep %d' % self.timeout
        self.log.info('### namespace')
        self.unshare = avocado.utils.process.SubProcess(cmdline)
        self.unshare.start()
        time.sleep(1)
        if self.unshare.poll() is not None:
            self.cancel("unshare failed to start")
        self.nsenter = '/usr/bin/nsenter -n -U --preserve-credentials -t %d' % self.unshare.get_pid()
        time.sleep(1)  # FIXME: wait for unshare init namespace

        self.log.info('### network setup')
        cmdline = 'tools/netinit.sh'
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        try:
            avocado.utils.process.run(cmdline)
        except:
            self.cancel("netinit failed")

    def tearDown(self):
        self.log.info('### stop processes')
        if self.webserver is not None:
            self.webserver.stop()
        if self.radvd is not None:
            self.radvd.stop()
        if self.dnsmasq is not None:
            self.dnsmasq.stop()
        if self.pcapture is not None:
            self.pcapture.stop()
        if self.unshare is not None:
            self.unshare.stop()
        Edk2TestCore.tearDown(self)

    def make_readable(self, filename):
        count = 0
        while not os.path.exists(filename):
            count += 1
            if count > 10:
                return
            time.sleep(0.1)
        os.chmod(filename, 0o644)

    def common_pcapture(self):
        if not os.path.exists("/usr/bin/dumpcap"):
            self.log.info('### skip packet capture (dumpcap not installed)')
            return
        self.log.info('### start packet capture')
        tapfile = self.outputdir + '/' + self.dev + '.pcap'
        cmdline = 'dumpcap'
        cmdline += ' -i ' + self.dev
        cmdline += ' -w ' + tapfile
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.pcapture = avocado.utils.process.SubProcess(cmdline)
        self.pcapture.start()
        self.make_readable(tapfile)

    def common_dnsmasq(self, append = None,
                       httpurl = None,
                       useipv6 = False):
        self.log.info('### start dnsmasq')
        logfile = self.logdir + "/dnsmasq.log"
        pidfile = self.workdir + "/dnsmasq.pid"
        cmdline = 'tools/dnsmasq.sh'
        cmdline += ' --log-facility=' + logfile
        cmdline += ' --pid-file=' + pidfile

        if httpurl:
            if useipv6:
                cmdline += ' --dhcp-boot=/404'                  # fail fast (pxe)
                cmdline += ' --dhcp-option-force=60,HTTPClient' # fail fast (ipv4)
                cmdline += ' --dhcp-option=option6:bootfile-url,' + httpurl
                cmdline += ' --dhcp-option-force=option6:16,2312HTTPClient'
            else:
                cmdline += ' --dhcp-boot=' + httpurl
                cmdline += ' --dhcp-option-force=60,HTTPClient'

        if not append == None:
            cmdline += ' ' + append
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.dnsmasq = avocado.utils.process.SubProcess(cmdline,
                                                        verbose = False)
        self.dnsmasq.start()
        self.make_readable(logfile)

    def common_radvd(self):
        self.log.info('### start radvd')
        logfile = self.logdir + "/radvd.log"
        cmdline = 'tools/radvd.sh'
        cmdline += ' --logmethod logfile'
        cmdline += ' --logfile ' + logfile
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.radvd = avocado.utils.process.SubProcess(cmdline)
        self.radvd.start()
        self.make_readable(logfile)

    def common_http(self, dir, addr):
        self.log.info('### start webserver')
        cmdline = 'python3 tools/webserver.py'
        cmdline += ' --directory=' + dir
        cmdline += ' --bind=' + addr
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.webserver = avocado.utils.process.SubProcess(cmdline)
        self.webserver.start()

    def common_https(self, dir, addr):
        self.log.info('### start secure webserver')
        mkcert = "tools/makecert.sh " + self.workdir
        avocado.utils.process.run(mkcert, verbose = True)

        self.vm.add_args('-object', 'tls-cipher-suites,id=tls-cipher0,priority=@SYSTEM')
        self.vm.add_args('-fw_cfg', 'name=etc/edk2/https/ciphers,gen_id=tls-cipher0')
        self.vm.add_args('-fw_cfg', 'name=etc/edk2/https/cacerts,file=' + self.workdir + '/ca.db')

        cmdline = 'python3 tools/webserver.py'
        cmdline += ' --directory=' + dir
        cmdline += ' --bind=' + addr
        cmdline += ' --tls'
        cmdline += ' --cert=' + self.workdir + '/server-chain.cert'
        cmdline += ' --key=' + self.workdir + '/server.key'
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.webserver = avocado.utils.process.SubProcess(cmdline)
        self.webserver.start()

    def common_add_tap_net(self, name = 'net', tap = 'tap0'):
        netdev = 'tap,id=' + name
        netdev += ',ifname=' + tap
        netdev += ',script=/bin/true'
        netdev += ',downscript=/bin/true'
        self.vm.add_args('-netdev', netdev)
