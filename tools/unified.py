#!/usr/bin/python
import os
import sys
import struct
import optparse
import tempfile
import subprocess

layout = {
    '.osrel'   : 0x1000000,
    '.cmdline' : 0x1100000,
    '.dtb'     : 0x1200000,
    '.splash'  : 0x1800000,
    '.linux'   : 0x2000000,
    '.initrd'  : 0x3000000,
}

def write_tmpfile(string):
    tmpfile = tempfile.NamedTemporaryFile()
    tmpfile.write(string.encode())
    tmpfile.flush()
    return tmpfile

def add_section(secname, filename):
    vma = layout[secname]
    cmdline = [ '--add-section', f'{secname}={filename}',
                '--change-section-vma', f'{secname}=0x{vma:x}' ]
    return cmdline

def detect_efi_arch(kernel):
    f = open(kernel, 'rb')
    header = f.read()
    (magic, offset) = struct.unpack_from('<H58xL', header)
    if magic != 0x5a4d:
        print(f'{kernel}: MZ magic missing')
        return None
    (magic, arch) = struct.unpack_from('<LH', header, offset)
    if magic != 0x4550:
        print(f'{kernel}: PE magic missing')
        return None
    if arch == 0x8664:
        return 'x64'
    if arch == 0xaa64:
        return 'aa64'
    print(f'{kernel}: unknown arch: 0x{arch:x}')
    return None

def main():
    parser = optparse.OptionParser()
    parser.add_option('-o', '--output', dest = 'output', type = 'string',
                      help = 'write unified efi binary to FILE.', metavar = 'FILE')
    parser.add_option('--osrel', '--name', dest = 'name', type = 'string', default = 'linux')
    parser.add_option('--append', '--cmdline', dest = 'cmdline', type = 'string')
    parser.add_option('--stub', dest = 'stub', type = 'string')
    parser.add_option('-l', '--linux', '--kernel', dest = 'linux', type = 'string',
                      help = 'read linux kernel from FILE.', metavar = 'FILE')
    parser.add_option('--initrd', dest = 'initrd', type = 'string',
                      help = 'read linux initrd from FILE.', metavar = 'FILE')
    (options, args) = parser.parse_args()

    if not options.output:
        print('output not specified (try --help)')
        return 1;
    if not options.linux:
        print('linux kernel not specified (try --help)')
        return 1;

    cmdline = [ 'objcopy' ]

    if not options.stub:
        arch = detect_efi_arch(options.linux)
        if arch is None:
            raise ValueError('detecting architecture failed')
        options.stub = f'/usr/lib/systemd/boot/efi/linux{arch}.efi.stub'
        
    if options.name:
        nfile = write_tmpfile(options.name)
        cmdline += add_section('.osrel', nfile.name)

    if options.cmdline:
        cfile = write_tmpfile(options.cmdline)
        cmdline += add_section('.cmdline', cfile.name)

    if options.linux:
        cmdline += add_section('.linux', options.linux)

    if options.initrd:
        cmdline += add_section('.initrd', options.initrd)

    cmdline += [ options.stub ]
    cmdline += [ options.output ]

    print(cmdline)
    subprocess.run(cmdline)

    cmdline = [ 'objdump', '-h', options.output ]
    subprocess.run(cmdline)

if __name__ == '__main__':
    sys.exit(main())
