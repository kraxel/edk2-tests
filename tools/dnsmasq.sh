#!/bin/sh

# cfg
dev="net0"
dn4="boot.v4.net.work"
dn6="boot.v6.net.work"
ip4="192.168.42.1"
ip6="fd42::1"

# go!
min4="${ip4%.*}.10"
max4="${ip4%.*}.99"
min6="${ip6%::*}::10"
max6="${ip6%::*}::ff"

if false; then
    # assign static ip address
    fix4="${ip4%.*}.200"
    fix6="${ip6%::*}::200"
    args="--dhcp-host=52:54:00:12:34:56,qemu.network,${fix4},[${fix6}/126]"
fi

set -x
exec /usr/sbin/dnsmasq \
    --no-daemon \
    --keep-in-foreground \
    --leasefile-ro \
    --log-dhcp \
    --log-queries \
    --interface $dev \
    --host-record=${dn4},${ip4} \
    --host-record=${dn6},${ip6} \
    --dhcp-range=${min4},${max4} \
    --dhcp-range=${min6},${max6} \
    $args \
    "$@"
