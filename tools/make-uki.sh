#!/bin/sh

dir="$1"

if test "$dir" = "" -o ! -d "$dir/EFI"; then
    echo "usage: $0 <dir>"
    exit 1
fi

uki="$dir/uki.efi"
kernel="$(echo $dir/*Image)"
initrd="$(echo $dir/rootfs*.xz)"
append="$(cat $dir/cmdline.txt)"

echo "# create $uki"
rm -v -f "$uki"
set -ex
/usr/lib/systemd/ukify       \
       --output   "$uki"     \
       --cmdline  "$append"  \
       "$kernel"  "$initrd"
