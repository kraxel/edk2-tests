#!/bin/sh

me="${0##*/}"
dir="$1"

if test "$dir" = "" -o ! -d "$dir/EFI"; then
    echo "usage: $0 <dir>"
    exit 1
fi

echo "### cleanup in $dir"
rm -v -f "$dir/boot."{img,iso}

case "$me" in
    *udf*)
        #
        # not working (grub fails to find grub.cfg @ udf)
        #
        echo "### create $dir/boot.iso [udf]"
        mkisofs \
            -udf -J \
            -o "$dir/boot.iso" \
            \
            -graft-points \
            EFI="$dir/EFI" \
            "$dir/"*Image \
            "$dir/"rootfs*.xz
        ;;

    *)
        echo "### create $dir/boot.img [vfat]"
        $(dirname $0)/vfat-builder "$dir/boot.img" "$dir/EFI" "/EFI"

        echo "### create $dir/boot.iso [iso9660/eltorito]"
        xorrisofs \
            -eltorito-boot boot.cat \
            -e "boot.img" \
            -no-emul-boot \
            -o "$dir/boot.iso" \
            \
            -graft-points \
            "$dir/boot.img" \
            EFI="$dir/EFI" \
            "$dir/"*Image \
            "$dir/"rootfs*.xz
        ;;

esac
