#!/bin/bash

# args
repo="$1"
disk="$2"
kick="$3"
pass="$4"

if test "$repo" = "" -o "$disk" = ""; then
    echo "usage: $0 <repourl> <image.qcow2> <kickstart> [ <rootpw> ]"
    exit 1
fi

# config
arch=$(uname -m)
case "$arch" in
    x86_64)
	code="/usr/share/edk2/ovmf/OVMF_CODE.secboot.fd"
	vars="/usr/share/edk2/ovmf/OVMF_VARS.fd"
	serial="ttyS0"
	machine="q35,smm=on"
	cpu="host"
	bootefi="BOOTX64.EFI"
	;;
    aarch64)
	code="/usr/share/edk2/aarch64/QEMU_EFI-silent-pflash.raw"
	vars="/usr/share/edk2/aarch64/vars-template-pflash.raw"
	serial="ttyAMA0"
	machine="virt,gic-version=host"
	cpu="host"
#	bootefi="BOOTAA64.EFI"
	bootefi="grubaa64.efi"
	;;
    *)
	echo "unknown arch: $arch"
	exit 1
	;;
esac

# find qemu
qemu=""
for item in /usr/bin/qemu-kvm /usr/libexec/qemu-kvm /usr/bin/qemu-system-$(uname -m); do
    if test -x "$item"; then qemu="$item"; break; fi
done
if test "$qemu" = ""; then
    echo "qemu not found"
    exit 1
fi

# work dir
WORK="$(mktemp -d /var/tmp/inst.XXXXXXXX)"
trap "rm -rf $WORK" EXIT

# boot files
boot="$WORK/boot"
mkdir $boot || exit 1
echo "# fetching from $repo"
echo "#  - efi files ..."
(cd $boot; wget -e robots=off -r -q -np -nH -nd \
		--reject-regex "\?.=.;.=." \
		"$repo/EFI/BOOT/")
echo "#  - kernel+initrd ..."
(cd $boot; wget -e robots=off -r -q -np -nH -nd \
		--reject-regex "\?.=.;.=." \
		"$repo/images/pxeboot/")

# kickstart
echo "# prepare kickstart (${kick}) ..."
cp "$kick" "$WORK/ks.cfg"
if test "$pass" = ""; then pass="$(cat distros/admin-password)"; fi
sed -i -e "s|rootpw.*|rootpw --plaintext ${pass}|" "$WORK/ks.cfg"
(cd $WORK; echo "ks.cfg" | cpio -H newc -o --file=boot/ks.cpio)

# grub config
echo "# prepare grub.cfg ..."
cat <<EOF > "$boot/grub.cfg"
echo ''
echo 'grub.efi started now'
echo ''
echo 'installing from repo'
echo '${repo}'
echo ''
echo 'loading kernel (vmlinuz) ...'
linux vmlinuz console=${serial} inst.repo=${repo} inst.ks=file:/ks.cfg
echo 'loading initrd (initrd.img, ks.cpio) ...'
initrd initrd.img ks.cpio
echo 'booting kernel ...'
boot
EOF

echo "# boot files:"
ls -ltr $boot

# copy vars
cp "$vars" "$WORK/vars.raw"
vars="$WORK/vars.raw"

# init disk
echo "# prepare disk (${disk}) ..."
qemu-img create -f qcow2 "$disk" 8G || exit 1

# run install
echo "# start $qemu ..."
$qemu \
    -machine "$machine" \
    -m 4G \
    -enable-kvm \
    -cpu "$cpu" \
    -nographic \
    -no-reboot \
    \
    -blockdev "node-name=code,driver=file,filename=${code},read-only=on" \
    -blockdev "node-name=vars,driver=file,filename=${vars}" \
    -blockdev "node-name=disk,driver=qcow2,file.driver=file,file.filename=${disk}" \
    -netdev "user,id=net0,tftp=${boot},bootfile=${bootefi}" \
    \
    -machine "pflash0=code,pflash1=vars" \
    -device "virtio-net-pci,netdev=net0" \
    -device "virtio-scsi-pci" \
    -device "scsi-hd,drive=disk"
