#!/bin/sh

# args
mode="$1"
disk="$2"
arch=$(uname -m)

if test "$mode" = "" -o "$disk" = ""; then
    echo "usage: $0 <mode> <image.qcow2>"
    exit 1
fi

# download urls
fedora35="https://download.fedoraproject.org/pub/fedora/linux/releases/35/"
stream8="http://mirror.centos.org/centos/8-stream/"
stream9="http://mirror.stream.centos.org/9-stream/"
case "$(hostname -f)" in
    *.kraxel.org)
	echo "### url override for kraxel.org"
	fedora35="http://spunk.home.kraxel.org/mirror/fedora/rsync/f35-release/"
	stream8="http://spunk.home.kraxel.org/centos/8-stream/"
	stream9="http://spunk.home.kraxel.org/stream/9-stream/"
	;;
    *.redhat.com)
	echo "### url override for redhat.com"
	fedora35="http://download.devel.redhat.com/released/fedora/F-35/GOLD/"
	stream8="http://download.devel.redhat.com/released/CentOS/centos/8-stream/"
	;;
esac

# go install
case "$mode" in
    fedora-35)
	echo "### install fedora 35"
	./tools/install-kickstart.sh \
	    "${fedora35}Server/${arch}/os/" \
	    "$disk" "kickstart/fedora-${arch}.ks"
	;;
    stream-8)
	echo "### install centos stream 8"
	./tools/install-kickstart.sh \
	    "${stream8}BaseOS/${arch}/os/" \
	    "$disk" "kickstart/stream8-${arch}.ks"
	;;
    stream-9)
	echo "### install centos stream 9"
	./tools/install-kickstart.sh \
	    "${stream9}BaseOS/${arch}/os/" \
	    "$disk" "kickstart/stream9-${arch}.ks"
	;;
    *)
	echo "unknown mode: $mode"
	exit 1
	;;
esac
