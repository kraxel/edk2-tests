#!/bin/sh

# cfg
dev="net0"
ip4="192.168.42.1"
ip6="fd42::1"

cfg=$(mktemp ${AVOCADO_TEST_WORKDIR-/tmp}/radv-XXXXXXXX.cfg)
pid=$(mktemp ${AVOCADO_TEST_WORKDIR-/tmp}/radv-XXXXXXXX.pid)

cat <<EOF > "$cfg"
interface $dev {
  AdvSendAdvert on;
  AdvManagedFlag on;
  AdvOtherConfigFlag on;
  AdvRASolicitedUnicast on;
  AdvLinkMTU 1500;
  prefix ${ip6%::*}::/64 {
    AdvAutonomous off;
    AdvOnLink on;
  };
};
EOF

set -x
exec radvd --nodaemon --config "$cfg" --pidfile "$pid" "$@"
