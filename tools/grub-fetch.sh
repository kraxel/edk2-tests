#!/bin/sh

# args
dest="$1"
arch="${2-$(uname -m)}"

# config
base="http://mirror.stream.centos.org/9-stream/"
repo="${base}BaseOS/${arch}/os/"
boot="${repo}EFI/BOOT/"

if test "$dest" = ""; then
    echo "usage: $0 <dest> [ <arch> ]"
    exit 1
fi

set -ex
cd $dest
wget -e robots=off -c -r -nv -np -nH -nd \
     --reject-regex "\?.=.;.=." \
     -A "*.efi,*.EFI" \
     "$boot"
ls -l
