#!/bin/sh
if test -x "$(which pwgen 2>/dev/null)"; then
    pwgen 12 1
else
    dd if=/dev/urandom bs=12 count=1 status=none | base64 
fi
