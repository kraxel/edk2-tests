#
# ovmf test cases, using network namespaces, x64
#

import os
import time
import logging
import subprocess
import grp

# avocado
import avocado
import avocado.utils.process

# local
from Edk2TestLib.core import Edk2TestCore
from Edk2TestLib.network import Edk2TestNetwork

# config
dn4 = "boot.v4.net.work"
dn6 = "boot.v6.net.work"
ip4 = "192.168.42.1"
ip6 = "fd42::1"


@avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNetNS(Edk2TestNetwork):

    timeout = 180

    def setUp(self):
        Edk2TestNetwork.setUp(self)

    def tearDown(self):
        self.log.info('### --- teardown ---')
        Edk2TestNetwork.tearDown(self)

    def common_prepare_x64_net(self, machine):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_tap_net('boot')

    def common_openssl_connect(self, hostname):
        time.sleep(1)
        self.log.info(f'### try openssl connect ({hostname})')
        cmd = 'echo -ne "GET / HTTP/1.0\\r\\n\\r\\n" | '
        cmd += self.nsenter + ' openssl s_client'
        cmd += ' -CAfile ' + self.workdir + '/ca.cert'
        cmd += ' -connect ' + hostname + ':8080'
        avocado.utils.process.run(cmd, shell = True)
        
    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grubx64.efi'), 'no grub.efi')
    def test_ovmf_q35_tftp4_grub(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:tftp
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        self.common_dnsmasq('--dhcp-boot=/EFI/BOOT/grubx64.efi'
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/uki.efi'), 'no uki.efi')
    def test_ovmf_q35_tftp4_uki(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:tftp
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        self.common_dnsmasq('--dhcp-boot=/uki.efi'
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grubx64.efi'), 'no grub.efi')
    def test_ovmf_q35_tftp6_grub(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:tftp
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'tftp://[' + ip6 + ']/EFI/BOOT/grubx64.efi'
        self.common_radvd()
        self.common_dnsmasq('--dhcp-boot=/404' # make PXEv4 fail fast
                            + ' --dhcp-option=option6:bootfile-url,' + url
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/uki.efi'), 'no uki.efi')
    def test_ovmf_q35_tftp6_grub(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:tftp
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'tftp://[' + ip6 + ']/uki.efi'
        self.common_radvd()
        self.common_dnsmasq('--dhcp-boot=/404' # make PXEv4 fail fast
                            + ' --dhcp-option=option6:bootfile-url,' + url
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grubx64.efi'), 'no grub.efi')
    def test_ovmf_q35_http4_dir(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn4 + ':8080/EFI/BOOT/grubx64.efi'
        self.common_dnsmasq(httpurl = url)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/uki.efi'), 'no uki.efi')
    def test_ovmf_q35_http4_uki(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn4 + ':8080/uki.efi'
        self.common_dnsmasq(httpurl = url)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_http4_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn4 + ':8080/boot.iso'
        self.common_dnsmasq(httpurl = url)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_https4_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:https
        :avocado: tags=protocol:ipv4
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'https://' + dn4 + ':8080/boot.iso'
        self.common_dnsmasq(httpurl = url)
        self.common_https(os.getcwd() + '/buildroot/initrd-x86_64', ip4)
        #self.common_openssl_connect(dn4)

        self.common_boot_disk()

    def test_ovmf_q35_http6_dir(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn6 + ':8080/EFI/BOOT/grubx64.efi'
        self.common_radvd()
        self.common_dnsmasq(httpurl = url, useipv6 = True)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', '::')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/uki.efi'), 'no uki.efi')
    def test_ovmf_q35_http6_uki(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn6 + ':8080/uki.efi'
        self.common_radvd()
        self.common_dnsmasq(httpurl = url, useipv6 = True)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', '::')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_http6_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:http
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn6 + ':8080/boot.iso'
        self.common_radvd()
        self.common_dnsmasq(httpurl = url, useipv6 = True)
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', '::')

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_https6_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=netboot:https
        :avocado: tags=protocol:ipv6
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'https://' + dn6 + ':8080/boot.iso'
        self.common_dnsmasq(httpurl = url, useipv6 = True)
        self.common_https(os.getcwd() + '/buildroot/initrd-x86_64', '::')
        #self.common_openssl_connect(dn6)

        self.common_boot_disk()
