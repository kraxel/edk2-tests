#!/bin/sh

opts="--max-parallel-tasks 1"	# new runner

set -ex
make distros/admin-password
avocado run $opts install.py -t "arch:$(uname -m)"
