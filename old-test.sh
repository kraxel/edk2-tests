#!/bin/sh

# use firmware images built with build.sh
export WORKSPACE=$(pwd)

avocado run --max-parallel-tasks 4 \
	bios.py $(ls *-{aa64,arm,ia32,x64,ia32x64,riscv64}.py) \
	"$@"
result="$?"
echo "# RESULT: $result"
#killall qemu-kvm qemu-system-i386 qemu-system-aarch64 qemu-system-arm dnsmasq radvd swtpm
exit $result
