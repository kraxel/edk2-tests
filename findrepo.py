#!/usr/bin/python
import os
import sys
import optparse

default = {
    'fedora'  : 'https://download.fedoraproject.org/pub/fedora/linux/releases/%s/',
    'stream9' : 'http://mirror.stream.centos.org/%s-stream/',
    'alpine'  : 'https://dl-cdn.alpinelinux.org/alpine/v%s/'
}

kraxel = {
    'fedora'  : 'http://hooch.home.kraxel.org/mirror/fedora/rsync/f%s-release/',
    'stream9' : 'http://distro.home.kraxel.org/%s-stream/',
    'rhel8'   : 'http://sirius.home.kraxel.org/rhel-8/rel-eng/RHEL-8/latest-RHEL-%s/compose/',
    'rhel9'   : 'http://sirius.home.kraxel.org/rhel-9/rel-eng/RHEL-9/latest-RHEL-%s/compose/',
    'alpine'  : 'http://distro.home.kraxel.org/alpine/v%s/'
}

redhat = {
    'fedora'  : 'http://download.devel.redhat.com/released/fedora/F-%s/GOLD/',
    'rhel8'   : 'http://download.devel.redhat.com/released/RHEL-8/%s/',
    'rhel9'   : 'http://download.devel.redhat.com/released/RHEL-9/%s/',
}

def getoverride():
    hostname = os.uname()[1]
    if 'kraxel.org' in hostname:
        return kraxel
    if 'redhat.com' in hostname:
        return redhat
    return {}

def defaultver(distro):
    if distro == 'fedora':
        return '41'
    if distro == 'stream9':
        return '9'
    if distro == 'rhel8':
        return '8.10.0'
    if distro == 'rhel9':
        return '9.5.0'
    if distro == 'alpine':
        return '3.21'
    else:
        return None

def findrepo(distro, version = None):
    if version is None:
        version = defaultver(distro)
    hostname = os.uname()[1]
    override = getoverride()
    if distro in override.keys():
        return override[distro] % version
    elif distro in default.keys():
        return default[distro] % version
    return None

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-d', '--distro', dest = 'distro', type = 'string',
                      help = 'find repo for DISTRO', metavar = 'DISTRO')
    parser.add_option('-v', '--version', dest = 'version', type = 'string',
                      help = 'use distro version VERSION', metavar = 'VERSION')
    parser.add_option('-l', '--list', dest = 'listmode', action = 'store_true', default = False,
                      help = 'list known distros')
    (options, args) = parser.parse_args()

    if options.listmode:
        override = getoverride()
        for item in override.keys():
            print(f"override: {item}")
        for item in default.keys():
            print(f"default: {item}")
        sys.exit(0)

    if options.distro is None:
        print("no distro given (use -d or --distro)")
        sys.exit(1)      

    if options.version is None:
        options.version = defaultver(options.distro)
    if options.version is None:
        print("no version given (use -v or --version)")
        sys.exit(1)      

    print(findrepo(options.distro, options.version))
