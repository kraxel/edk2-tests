#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 60

    def test_ovmf_x64_pc_rom_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc', memory = '1G')
        self.common_add_rom('Firmware/x64/OVMF.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    def test_ovmf_x64_pc_rom_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc', memory = '4G')
        self.common_add_rom('Firmware/x64/OVMF.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    def test_ovmf_x64_pc_flash_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc', memory = '1G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_pc_flash_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc', memory = '4G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_rom_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '1G')
        self.common_add_rom('Firmware/x64/OVMF.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_rom_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '4G')
        self.common_add_rom('Firmware/x64/OVMF.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_flash_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '1G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_flash_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '4G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_smm_flash_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '1G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.secboot.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_q35_smm_flash_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '4G')
        self.common_add_flash('Firmware/x64/OVMF_CODE.secboot.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_flash()
        self.common_check_efi_smp()

    def test_ovmf_x64_microvm_rom_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'microvm,rtc=on', memory = '1G')
        self.common_add_rom('Firmware/x64/MICROVM.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    def test_ovmf_x64_microvm_rom_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'microvm,rtc=on', memory = '4G')
        self.common_add_rom('Firmware/x64/MICROVM.fd')
        self.common_boot_kernel()
        self.common_check_efi_vars_emu()
        self.common_check_efi_smp()

    @avocado.skip('requires sev') # needs kernel hashes
    def test_ovmf_x64_q35_amd_sev_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '1G')
        self.common_add_rom('Firmware/x64/OVMF.amdsev.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_q35_intel_tdx_1g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '1G')
        self.common_add_rom('Firmware/x64/OVMF.inteltdx.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_q35_intel_tdx_4g(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35', memory = '4G')
        self.common_add_rom('Firmware/x64/OVMF.inteltdx.fd')
        self.common_boot_kernel()
