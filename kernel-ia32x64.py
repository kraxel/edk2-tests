#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 60

    def test_ovmf_ia32x64_q35_flash(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35')
        self.common_add_flash('Firmware/x64/OVMF_CODE.secboot.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        self.common_boot_kernel()
