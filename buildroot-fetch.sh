#!/bin/sh
name="$1"
page="$2"

mkdir -p $(dirname $0)/buildroot
cd $(dirname $0)/buildroot

pattern="${name}.tar.xz"

rm -f "$pattern" robots.txt.*
echo "#"
echo "# fetching buildroot: $name"
echo "#"
wget -nv -nd -r -H -D gitlab.com \
     -A "$pattern" \
     https://kraxel.gitlab.io/br-kraxel/qemu-${page}.html
