#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 120

    def test_aa64_flash_acpi(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('aarch64', 'virt,acpi=on')
        self.common_add_flash('Firmware/aa64/QEMU_EFI.fd',
                              'Firmware/aa64/QEMU_VARS.fd')
        self.common_boot_kernel()

    def test_aa64_flash_dt(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('aarch64', 'virt,acpi=off')
        self.common_add_flash('Firmware/aa64/QEMU_EFI.fd',
                              'Firmware/aa64/QEMU_VARS.fd')
        self.common_boot_kernel()
