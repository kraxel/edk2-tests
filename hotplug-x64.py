#
# ovmf test cases, hotplug, x64
#

import os
import time
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-x86_64/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60
    ports = []

    def common_prepare_x64(self, machine, ports):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')
        self.common_add_virtio_blk_pcie()
        for i in range(ports):
            self.ports.append(self.common_add_pcie_port())

    def common_prepare_x64_smm(self, machine):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Firmware/x64/OVMF_CODE.secboot.fd',
                              'Firmware/x64/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')
        self.common_add_virtio_blk_pcie()

    def common_hot_add(self, name, device, bus):
        self.vm.cmd('device_add',
                    id = name,
                    driver = device,
                    bus = bus)
        time.sleep(1)

    def test_ovmf_pcie_many_ports(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64('q35', 32)

        self.common_launch()
        self.common_linux_buildroot_init()

        self.console_send('lspci -vt')
        self.console_wait('---root---')

        self.common_linux_shutdown()

    def test_ovmf_pcie_hotplug_scsi(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=io:pcie
        :avocado: tags=hotplug:scsi
        """
        self.common_prepare_x64('q35', 4)

        self.common_launch()
        self.common_linux_buildroot_init()

        self.common_hot_add('hotplug', 'virtio-scsi-pci', self.ports[0])
        self.console_send('lspci -vt')
        self.console_wait('---root---')

        self.common_linux_shutdown()

    def test_ovmf_hotplug_vcpu(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=hotplug:cpu
        """
        self.common_prepare_x64_smm('q35')

        self.common_launch()
        self.common_linux_buildroot_init()

        cpus = self.vm.cmd('query-hotpluggable-cpus')
        names = []
        count = 0
        for cpu in cpus:
            self.log.info(f"### cpu: {cpu}")
            if cpu.get('qom-path'):
                self.log.info(f"### already online.")
                # already online
                continue
            count += 1
            name = f'hotcpu{count}'
            names.append(name)
            self.log.info(f"### hotplugging {name} ...")
            self.vm.cmd('device_add',
                        driver    = cpu['type'],
                        id        = name,
                        socket_id = cpu['props']['socket-id'],
                        core_id   = cpu['props']['core-id'],
                        thread_id = cpu['props']['thread-id'])
            time.sleep(3)

        for cpu in names:
            self.log.info(f"### unplugging {cpu} ...")
            self.vm.cmd('device_del', id = cpu)
            time.sleep(3)

        self.common_linux_shutdown()
