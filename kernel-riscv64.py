#
# ovmf test cases using direct kernel boot.
#

import os
import shutil
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 120

    def common_add_flash_riscv(self, image_code, image_vars):
        code = self.find_fw(image_code, "code")
        if os.path.getsize(code) != 32 * 1024 * 1024:
            code = self.pad_fw(code, 32)
            self.rmlist.append(code)

        tmpl = self.find_fw(image_vars, "vars")
        if os.path.getsize(tmpl) != 32 * 1024 * 1024:
            tmpl = self.pad_fw(tmpl, 32)
            self.rmlist.append(tmpl)

        copy = "%s/image.raw" % self.workdir
        self.rmlist.append(copy)
        shutil.copyfile(tmpl, copy)

        self.vm.add_args('-cpu', 'rv64')

        self.log.info("### code     : %s", code)
        self.log.info("### vars tmpl: %s", tmpl)
        self.log.info("### vars copy: %s", copy)
        self.vm.add_args('-drive', f'if=pflash,index=0,format=raw,file={code},readonly=on')
        self.vm.add_args('-drive', f'if=pflash,index=1,format=raw,file={copy}')

    def test_riscv64_flash(self):
        """
        :avocado: tags=arch:riscv64
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('riscv64', 'virt')
        self.common_add_flash_riscv('Firmware/riscv64/RISCV_VIRT_CODE.fd',
                                    'Firmware/riscv64/RISCV_VIRT_VARS.fd')
        self.common_boot_kernel()
