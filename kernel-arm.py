#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestLib.core import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 60

    def test_arm_flash_acpi(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('arm', 'virt,acpi=on')
        self.common_add_flash('Firmware/arm/QEMU_EFI.fd',
                              'Firmware/arm/QEMU_VARS.fd')
        self.common_boot_kernel()

    def test_arm_flash_dt(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('arm', 'virt,acpi=off')
        self.common_add_flash('Firmware/arm/QEMU_EFI.fd',
                              'Firmware/arm/QEMU_VARS.fd')
        self.common_boot_kernel()
