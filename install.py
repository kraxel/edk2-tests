#
# redhat distro installer
#

import os
import re
import json
import glob
import shutil
import logging
import tempfile
import configparser
import urllib.error
import urllib.parse
import urllib.request

# avocado
import avocado

# local
from findrepo import findrepo
from Edk2TestLib.core import Edk2TestCore, PASSWD_FILE, \
    FEDORA_X64, STREAM9_X64, \
    FEDORA_AA64, STREAM9_AA64, \
    RHEL8_X64, RHEL8_AA64, \
    RHEL9_X64, RHEL9_AA64, \
    ALPINE_X64, ALPINE_AA64, ALPINE_IA32, ALPINE_ARM

class DistroInstall(Edk2TestCore):

    timeout = int(os.environ.get("INSTALL_TIMEOUT", "1800"))

    def find_firmware(self):
        if os.environ.get('WORKSPACE') is not None:
            return
        if os.environ.get('EDK2_CODE') is not None:
            return

        list = glob.glob('/usr/share/qemu/firmware/*.json')
        list.sort()
        for item in list:
            f = open(item)
            fw = json.load(f)
            f.close()

            # hard requirements
            if fw['targets'][0]['architecture'] != self.arch:
                continue
            if fw['mapping']['device'] != 'flash':
                continue
            if fw['mapping']['executable']['format'] == 'qcow2':
                continue
            if 'uefi' not in fw['interface-types']:
                continue

            # no secure boot installs for now
            if 'enrolled-keys' in fw['features']:
                continue

            # no stateless firmware for now
            if 'mode' in fw['mapping'] and fw['mapping']['mode'] == 'stateless':
                continue

            self.log.info('### fw json: ' + item)
            self.log.info('### fw desc: ' + fw['description'])
            os.environ['EDK2_CODE'] = fw['mapping']['executable']['filename']
            os.environ['EDK2_VARS'] = fw['mapping']['nvram-template']['filename']
            return

    def common_prepare_machine(self):
        if self.arch == 'x86_64':
            self.common_prepare('x86_64', 'q35', memory = '2G')
            self.common_add_flash('Firmware/x64/OVMF_CODE.fd',
                                  'Firmware/x64/OVMF_VARS.fd')
            return

        if self.arch == 'i386':
            self.common_prepare('i386', 'q35')
            self.common_add_flash('Firmware/ia32/OVMF_CODE.fd',
                                  'Firmware/ia32/OVMF_VARS.fd')
            return

        if self.arch == 'aarch64':
            self.common_prepare('aarch64', 'virt', memory = '2G')
            self.common_add_flash('Firmware/aa64/QEMU_EFI.fd',
                                  'Firmware/aa64/QEMU_VARS.fd')
            return

        if self.arch == 'arm':
            self.common_prepare('arm', 'virt')
            self.common_add_flash('Firmware/arm/QEMU_EFI.fd',
                                  'Firmware/arm/QEMU_VARS.fd')
            return

        self.fail('unknown arch')

    def common_prepare_disk(self, image):
        self.log.info(f'### create disk {image} ...')
        cmdline = 'qemu-img create -f qcow2 ' + image + ' 8G'
        avocado.utils.process.run(cmdline)
        blkdev = 'node-name=disk'
        blkdev += ',driver=qcow2'
        blkdev += ',file.driver=file'
        blkdev += ',file.filename=' + image
        self.vm.add_args('-blockdev', blkdev)
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk(self.dev_disk)

    def fetch_file(self, repo, file):
        url = repo + file
        req = urllib.request.Request(url)
        self.log.info(f'### fetching {req.full_url} ...')
        try:
            res = urllib.request.urlopen(req)
            return res
        except urllib.error.URLError as err:
            self.log.error(f'### fetch {req.full_url}: {err}')
            self.fail('download failed')
        except Exception as err:
            self.fail('Oops: {err}')

    def prepare_tftp(self, repo):
        bootfiles = [
            f'EFI/BOOT/grub{self.efiarch}.efi',
            self.kernel,
            self.initrd,
        ]
        for item in bootfiles:
            res = self.fetch_file(repo, item)
            out = os.path.join(self.workdir, os.path.basename(item))
            f = open(out, 'wb')
            shutil.copyfileobj(res, f)
            f.close()
            self.rmlist.append(out)

    def prepare_grub_cfg(self, repo):
        out  = os.path.join(self.workdir, 'grub.cfg')
        post = ''
        inst = 'inst.'

        f = open(out, 'w')
        f.write('echo "grub.efi started"\n')
        f.write(f'echo "install {self.distro}"\n')
        f.write(f'echo "repo {repo}"\n')

        kernel = os.path.basename(self.kernel)
        f.write(f'echo "loading {kernel} ..."\n')
        f.write(f'linux{post} {kernel} console={self.serial} {inst}repo={repo} {inst}ks=file:/ks.cfg\n')

        initrd = os.path.basename(self.initrd)
        f.write(f'echo "loading {initrd} + ks.cpio ..."\n')
        f.write(f'initrd{post} {initrd} ks.cpio\n')

        f.write(f'echo "starting linux kernel ..."\n')
        f.write(f'boot\n')
        f.close()
        self.rmlist.append(out)

    def prepare_kickstart(self, kickstart):
        # read
        f = open(kickstart, 'r')
        cfg = f.read()
        f.close()

        # set password
        f = open(PASSWD_FILE, "r")
        password = f.read().strip()
        f.close()
        cfg = re.sub('rootpw[^\n]*', f'rootpw --plaintext {password}', cfg)

        # write
        out = os.path.join(self.workdir, 'ks.cfg')
        f = open(out, 'w')
        f.write(cfg)
        f.close()

        # wrap into cpio (for initramfs)
        cmdline = f'(cd {self.workdir}; echo ks.cfg | cpio -H newc -o --file=ks.cpio)'
        avocado.utils.process.run(cmdline, shell = True)
        self.rmlist.append(out)
        self.rmlist.append(os.path.join(self.workdir, 'ks.cpio'))

    def prepare_arch(self, arch):
        self.arch = arch
        self.dev_disk = 'scsi-hd'
        self.dev_cdrom = 'scsi-cd'
        if self.arch == 'x86_64':
            self.efiarch = 'x64'
            self.serial = 'ttyS0'
        if self.arch == 'i386':
            self.efiarch = 'ia64'
            self.serial = 'ttyS0'
            self.dev_disk = 'ide-hd,bus=ide.0'
            self.dev_cdrom = 'ide-cd,bus=ide.1'
        if self.arch == 'aarch64':
            self.efiarch = 'aa64'
            self.serial = 'ttyAMA0'
        if self.arch == 'arm':
            self.efiarch = 'arm'
            self.serial = 'ttyAMA0'

    def distro_info(self):
        self.prepare_arch(self.treeinfo['general']['arch'])

        self.distro = self.treeinfo['general']['name']
        self.kernel = self.treeinfo['images-' + self.arch]['kernel']
        self.initrd = self.treeinfo['images-' + self.arch]['initrd']
        self.log.info(f'### distro is {self.distro} ({self.arch})')

    def common_run_install(self, repo, image, kickstart):
        res = self.fetch_file(repo, '.treeinfo')
        self.treeinfo = configparser.ConfigParser(allow_no_value=True)
        self.treeinfo.read_string(res.read().decode('utf-8'))
        self.distro_info()
        self.prepare_tftp(repo)
        self.prepare_kickstart(kickstart)
        self.prepare_grub_cfg(repo)

        self.find_firmware()
        self.common_prepare_machine()
        self.vm.add_args('-m', '4G')

        (fh, imagetmp) = tempfile.mkstemp(None,
                                          os.path.basename(image) + '.',
                                          os.path.dirname(image))
        self.rmlist.append(imagetmp)
        os.close(fh)
        self.common_prepare_disk(imagetmp)

        self.common_add_user_net(tftp = self.workdir,
                                 bootfile = f'grub{self.efiarch}.efi')
        self.common_add_nic_pcie('virtio-net-pci',
                                 bootindex = 2)

        self.common_launch()
        self.console_wait('grub.efi started')
        self.log.info(f'### inst: grub loaded')

        self.console_wait('Linux version')
        self.log.info(f'### inst: linux kernel loaded')

        self.console_wait('Starting installer')
        self.log.info(f'### inst: installer loaded')

        self.console_wait('Starting automated install')
        self.log.info(f'### inst: install started')

        self.console_wait('Downloading packages')
        self.log.info(f'### inst: rpm downlaod')

        self.console_wait('Preparing transaction')
        self.log.info(f'### inst: rpm install')

        self.console_wait('post-installation setup')
        self.log.info(f'### inst: post install setup')

        self.console_wait('Storing configuration files')
        self.log.info(f'### inst: install finished') # almost

#        self.console_wait('Rebooting.')
#        self.log.info(f'### inst: reboot')
        self.console_wait('Linux version')
        self.log.info(f'### reboot: linux kernel loaded')

        self.common_linux_distro_login()
        self.common_linux_shell_init()

        for service in ('kdump.service', 'chronyd.service', 'firewalld.service'):
            self.console_send(f'systemctl disable {service}')
            self.console_wait('---root---')

        self.common_linux_shutdown()
        os.link(imagetmp, image)

    def common_alpine_install(self, arch, version, image, alpine_arch = None, variant = 'virt'):
        self.prepare_arch(arch)

        if alpine_arch is None:
            alpine_arch = arch
        mirror = findrepo('alpine', version)
        urlbase = f'{mirror}releases/{alpine_arch}/'
        kernelname  = f'netboot-{version}.0/vmlinuz-{variant}'
        initrdname  = f'netboot-{version}.0/initramfs-{variant}'

        kernelreq = self.fetch_file(urlbase, kernelname)
        kernel = os.path.join(self.workdir, 'kernel')
        with open(kernel, 'wb') as f:
            shutil.copyfileobj(kernelreq, f)

        initrdreq = self.fetch_file(urlbase, initrdname)
        initrd = os.path.join(self.workdir, 'initrd')
        with open(initrd, 'wb') as f:
            shutil.copyfileobj(initrdreq, f)

        cmdline  = f'console={self.serial}'
        cmdline += f' modloop={urlbase}/netboot-{version}.0/modloop-{variant}'
        cmdline += f' modules=loop,squashfs,sd-mod'
        cmdline += f' alpine_repo={mirror}/main/'

        self.find_firmware()
        self.common_prepare_machine()
        self.vm.add_args('-m', '4G')
        self.vm.add_args('-kernel', kernel)
        self.vm.add_args('-initrd', initrd)
        self.vm.add_args('-append', cmdline)

        (fh, imagetmp) = tempfile.mkstemp(None,
                                          os.path.basename(image) + '.',
                                          os.path.dirname(image))
        self.rmlist.append(imagetmp)
        os.close(fh)
        self.common_prepare_disk(imagetmp)

        self.common_launch()

        self.console_wait('Welcome to Alpine')
        self.log.info(f'### book ok')

        self.console_wait_char('login:')
        self.console_send('root')
        self.common_linux_shell_init()
        self.log.info(f'### login ok')

        self.console_send('setup-alpine -q -e </dev/null')
        self.console_wait('---root---')
        self.console_send('yes | USE_EFI=1 KERNELOPTS=console=' + self.serial + ' setup-disk -q -m sys /dev/sda | grep -v WARNING:')
        self.console_wait('---root---')
        self.log.info(f'### install ok')

        self.console_send('fdisk -l /dev/sda')
        self.console_wait('---root---')

        self.common_linux_shutdown()
        os.link(imagetmp, image)

    def common_alpine_boot(self, arch, image):
        self.prepare_arch(arch)

        self.find_firmware()
        self.common_prepare_machine()
        self.vm.add_args('-m', '4G')

        self.common_add_qcow2_disk(image)
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk(self.dev_disk)

        self.common_launch()
        self.console_wait('Welcome to Alpine')
        self.log.info(f'### book ok')

        self.console_wait_char('login:')
        self.console_send('root')
        self.common_linux_shell_init()
        self.log.info(f'### login ok')

        self.common_linux_shutdown()

    ####################################################################
    # x86_64

    @avocado.skipIf(os.path.exists(FEDORA_X64), 'fedora already installed')
    def test_fedora_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:fedora
        """
        self.common_run_install(findrepo('fedora') + 'Server/x86_64/os/',
                                FEDORA_X64, 'kickstart/fedora-x86_64.ks')

    @avocado.skipIf(os.path.exists(STREAM9_X64), 'centos stream 9 already installed')
    def test_stream9_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:stream9
        """
        self.common_run_install(findrepo('stream9') + 'BaseOS/x86_64/os/',
                                STREAM9_X64, 'kickstart/stream9-x86_64.ks')

    @avocado.skipIf(os.path.exists(RHEL8_X64), 'rhel 8 already installed')
    def test_rhel8_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:rhel8
        """
        self.common_run_install(findrepo('rhel8') + 'BaseOS/x86_64/os/',
                                RHEL8_X64, 'kickstart/stream8-x86_64.ks')

    @avocado.skipIf(os.path.exists(RHEL9_X64), 'rhel 9 already installed')
    def test_rhel9_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:rhel9
        """
        self.common_run_install(findrepo('rhel9') + 'BaseOS/x86_64/os/',
                                RHEL9_X64, 'kickstart/stream9-x86_64.ks')


    ####################################################################
    # aarch64

    @avocado.skipIf(os.path.exists(FEDORA_AA64), 'fedora already installed')
    def test_fedora_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:fedora
        """
        self.common_run_install(findrepo('fedora') + 'Server/aarch64/os/',
                                FEDORA_AA64, 'kickstart/fedora-aarch64.ks')

    @avocado.skipIf(os.path.exists(STREAM9_AA64), 'centos stream 9 already installed')
    def test_stream9_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:stream9
        """
        self.common_run_install(findrepo('stream9') + 'BaseOS/aarch64/os/',
                                STREAM9_AA64, 'kickstart/stream9-aarch64.ks')

    @avocado.skipIf(os.path.exists(RHEL8_AA64), 'rhel 8 already installed')
    def test_rhel8_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:rhel8
        """
        self.common_run_install(findrepo('rhel8') + 'BaseOS/aarch64/os/',
                                RHEL8_AA64, 'kickstart/stream8-x86_64.ks')

    @avocado.skipIf(os.path.exists(RHEL8_AA64), 'rhel 9 already installed')
    def test_rhel9_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:rhel9
        """
        self.common_run_install(findrepo('rhel9') + 'BaseOS/aarch64/os/',
                                RHEL9_AA64, 'kickstart/stream9-x86_64.ks')

    ####################################################################
    # alpine

    @avocado.skipIf(os.path.exists(ALPINE_X64), 'alpine x64 already installed')
    def test_alpine_x64_inst(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:alpine
        """
        self.common_alpine_install('x86_64', '3.21', ALPINE_X64)

    @avocado.skipUnless(os.path.exists(ALPINE_X64), 'alpine x64 not installed')
    def test_alpine_x64_boot(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:alpine
        """
        self.common_alpine_boot('x86_64', ALPINE_X64)


    @avocado.skipIf(os.path.exists(ALPINE_AA64), 'alpine aa64 already installed')
    def test_alpine_aa64_inst(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:alpine
        """
        self.common_alpine_install('aarch64', '3.21', ALPINE_AA64)

    @avocado.skipUnless(os.path.exists(ALPINE_AA64), 'alpine aa64 not installed')
    def test_alpine_aa64_boot(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:alpine
        """
        self.common_alpine_boot('aarch64', ALPINE_AA64)
